package question2;

public class Recursion {
	private static int count = 0;
	private static int position = 0;
	
	public static void main(String[]args) {
		int[] arr = new int[] {10,11,12,13,14,15,16,21,22,23,7,36,45,65,1,32,45,75,34};
		int n = 5;
		//Should be 6
		System.out.println(recursiveCount(arr,n));
	}
	
	public static int recursiveCount(int[] numbers,int n) {
		if(position < numbers.length && numbers[position] > 20 && position%2 != 0 && position >= n) {
			count++;
			position++;
			recursiveCount(numbers, n);
		}
		else if(position < numbers.length) {
			position++;
			recursiveCount(numbers, n);
		}
		
		return count;
	}
}
