package question3;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class Gui extends Application{
	private Bet game = new Bet();
	private int bet = 0;
	public void start(Stage stage) {
		Group root = new Group(); 
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 715, 300); 
		scene.setFill(Color.BLACK);
		
		//Setting a single hbox because of lack of time
		HBox hbox = new HBox();
		Button odd = new Button("Odd");
		Button even = new Button("Even");
		TextField money = new TextField("Money: 250");
		TextField betting = new TextField(""+ bet);
		TextField message = new TextField("Messages go here");
		hbox.getChildren().addAll(odd, even, betting, money, message);
		root.getChildren().add(hbox);
		
		//Adding events to the two buttons
		BetChoice betOdd = new BetChoice(betting, money, game, 1, message);
		BetChoice betEven = new BetChoice(betting, money, game, 2, message);
		odd.setOnAction(betOdd);
		even.setOnAction(betEven);
		
		//Setting the stage
		stage.setTitle("Betting on dice rolls");
		stage.setScene(scene);
		
		stage.show();
	}
	//Launching the gui
	public static void main(String[]args) {
		Application.launch(args);
	}
}
