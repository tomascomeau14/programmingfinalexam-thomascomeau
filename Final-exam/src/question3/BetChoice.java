package question3;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
public class BetChoice implements EventHandler<ActionEvent> {
	private Bet game;
	private TextField betting;
	private TextField money;
	private int choice;
	private TextField message;
	
	public BetChoice(TextField betting, TextField money, Bet game,int choice, TextField message) {
		this.betting = betting;
		this.money = money;
		this.game = game;
		this.choice = choice;
		this.message = message;
	}

	//Will handle the game gui
	@Override
	public void handle(ActionEvent e) {
		// TODO Auto-generated method stub
		String bet = betting.getText();
		int temp = Integer.parseInt(bet);
		if(game.valid(temp)) {
			message.setText("Valid bet");
		}
		else {
			message.setText("invalid bet");
		}
		game.betMoney(temp, choice);
		money.setText(game.getMoney()+"");
	}

}
