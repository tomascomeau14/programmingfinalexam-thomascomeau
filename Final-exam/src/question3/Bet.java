package question3;
import java.util.Random;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class Bet {

	//How much money the player has
	private static int money = 250;
	//A random object to simulate a dice roll
	private static Random random = new Random();	
	
	//Simulates a dice roll
	public int roll() {
		int roll = random.nextInt(6);
		return roll+1;
	}
	
	//returns the amount of money
	public int getMoney() {
		return money;
	}
	
	//Checks to see how mush is bet and wither you win or not
	public void betMoney(int bet, int choice) {
		if(choice == 2) {
			if(valid(bet)) {
				int roll = roll();
				int check = roll%2;
				if(check == 0) {
					money = money + bet;
				}
				else {
					money = money - bet;
				}
			}
		}
		else {
			if(valid(bet)) {
				int roll = roll();
				int check = roll%2;
				if(check != 0) {
					money = money + bet;
				}
				else {
					money = money - bet;
				}
			}
		}
	}
	
	//Checks to see if its a valid bet
	public boolean valid(int bet) {
		if(bet > money) {
			return false;
		}
		else if(bet == 0){
			return false;
		}
		else {
			return true;
		}
	}
}
